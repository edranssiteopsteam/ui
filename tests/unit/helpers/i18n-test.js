import { i18n } from 'starme-ui/helpers/i18n';
import { module, test } from 'qunit';

module('Unit | Helper | i18n');

// Replace this with your real tests.
test('it works', function(assert) {
  const result = i18n([42]);
  assert.ok(result);
});
