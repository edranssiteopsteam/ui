import { debugJson } from 'starme-ui/helpers/debug-json';
import { module, test } from 'qunit';

module('Unit | Helper | debug json');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = debugJson([42]);
  assert.ok(result);
});
