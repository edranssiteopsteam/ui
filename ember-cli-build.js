/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var customOptions = {
    fingerprint: {
      exclude: [ 'assets/images/emails/**/*' ]
    }
  };

  var app = new EmberApp(defaults, customOptions);

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  if (app.env === 'development') {
    app.import('bower_components/bootstrap/dist/js/bootstrap.js');
    app.import('bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.css');
    app.import('bower_components/bootstrap-material-design/dist/js/ripples.js');
    app.import('bower_components/material-design-color-palette/css/material-design-color-palette.css');
    app.import('bower_components/bootstrap-material-design/dist/js/material.js');
    app.import('bower_components/bootstrap-material-design/dist/js/ripples.js');
  } else {
    app.import('bower_components/bootstrap/dist/js/bootstrap.min.js');
    app.import('bower_components/bootstrap-material-design/dist/css/bootstrap-material-design.min.css');
    app.import('bower_components/bootstrap-material-design/dist/css/ripples.min.css');
    app.import('bower_components/material-design-color-palette/css/material-design-color-palette.min.css');
    app.import('bower_components/bootstrap-material-design/dist/js/material.min.js');
    app.import('bower_components/bootstrap-material-design/dist/js/ripples.min.js');
  }

  app.import('vendor/flaticon.css');
  app.import('vendor/material-init.js');

  return app.toTree();
};
