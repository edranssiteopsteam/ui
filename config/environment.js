/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'starme-ui',
    environment: environment,
    baseURL: '/',
    locationType: 'hash',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      ENDPOINT_API: '/api',
      AUTH_ENDPOINT: '/api/auth/login',
      GOOGLE_CLIENT_ID: '505476297933-tv8qnl863mo2diegud8h6n96bsomqqjk.apps.googleusercontent.com',
      GOOGLE_CODE_ENDPOINT: '/api/google/postmessage',
      USER_DEFAULT_IMAGE: '/assets/images/empty-user.jpeg',
      LOGIN_NOTIFICATION_TIME: 1000
    },

    i18n: {
      defaultLocale: 'es'
    }
  };

  ENV['ember-simple-auth'] = {
    authenticationRoute: 'login',
    routeAfterAuthentication: 'dashboard',
    routeIfAlreadyAuthenticated: 'dashboard'
  };

  if (environment === 'development') {
    ENV.APP.ENDPOINT_API = 'http://127.0.0.1:8080';
    ENV.APP.AUTH_ENDPOINT = 'http://127.0.0.1:8080/auth/login';
    ENV.APP.GOOGLE_CODE_ENDPOINT = 'http://127.0.0.1:8080/google/postmessage';
    ENV.APP.GOOGLE_CLIENT_ID = '505476297933-u1dlpeqfnoqqtb7ca2abj5mtf9uuhp2v.apps.googleusercontent.com';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  // here you can check if environment is 'production'

  return ENV;
};
