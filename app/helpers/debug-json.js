import Ember from 'ember';

export default Ember.Helper.helper((o) => {
  return JSON.stringify(o, null, '  ');
});
