import Ember from 'ember';

export default Ember.Service.extend({
  getName(userData) {
    const first = userData.first_name;
    const last = userData.last_name;
    const email = userData.email;
    return first ? first + ' ' + last : email.split('@')[0];
  }
});
