import Ember from 'ember';
import JSONAPIAdapter from 'ember-data/adapters/json-api';
import ENV from '../config/environment';

export default JSONAPIAdapter.extend({
  session: Ember.inject.service(),

  host: ENV.APP.ENDPOINT_API,

  headers: Ember.computed('session.data.authenticated.access_token', function() {
    return {
      'Accept': 'application/json',
      'Authorization': `Bearer ${this.get('session.data.authenticated.access_token')}`
    };
  }),

  buildURL(modelName, id, snapshot, requestType, query) {
    if (modelName === 'user' && isEmpty(query)) {
      return `${this.get('host')}/users/me`;
    }

    return this._super(...arguments);
  }
});

function isEmpty(o) {
  return Object.keys(o).length === 0 && o.constructor === Object;
}
