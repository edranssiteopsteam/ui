import Ember from 'ember';

export default Ember.Component.extend({
  didReceiveAttrs() {
    const amount = this.get('amount');
    this.set('remaining.progress', amount / 10);
  },

  remaining: {
    progress: 0,
    options: {
      color: '#E91E63',
      strokeWidth: 3,
      text: {
        className: 'remaining-star-label',
        value: '0'
      },
      trailColor: '#eee',
      trailWidth: 3,
      step: function(state, circle) {
        var value = Math.round(circle.value() * 10);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value);
        }
      }
    }
  }
});
