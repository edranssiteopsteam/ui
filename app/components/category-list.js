import Ember from 'ember';

export default Ember.Component.extend({
  init() {
    this._super(...arguments);
    this.isAssigningCategory = false;
    this.category = null;
  },

  didInsertElement() {
    this.$('[data-toggle="tooltip"]').tooltip();
  },

  actions: {
    select(category) {
      this.set('category', category);
    },

    close() {
      this.set('category', null);
    }
  }
});
