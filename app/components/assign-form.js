import Ember from 'ember';

export default Ember.Component.extend({
  notify: Ember.inject.service(),
  i18n: Ember.inject.service(),

  actions: {
    assign() {
      const from = this.get('user.email');
      const category = this.get('category.id');
      const { assignee, comment } = this.getProperties('assignee', 'comment');
      const assignment = this.get('store').createRecord('assignment', {
        date: null,
        from: from,
        to: assignee,
        category: category,
        comment: comment
      });

      const i18n = this.get('i18n');
      assignment.save().then(() => {
        this.get('notify').success(i18n.t('assign.success', {user: assignee, category: i18n.t(this.get('category.title'))}));
        this.sendAction('close');
        window.location.reload(true); //TODO: add the new assignment to the list, don't reload the whole page
      }, () => {
        this.get('notify').alert(i18n.t('assign.error'));
      });
    }
  }
});
