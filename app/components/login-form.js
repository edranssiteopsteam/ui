import Ember from 'ember';
import ENV from '../config/environment';

export default Ember.Component.extend({
  session: Ember.inject.service(),
  notify: Ember.inject.service(),
  i18n: Ember.inject.service(),

  gapi_auth2: null,

  willRender() {
    if (window.gapi) {
      if (!window.gapi.auth2) {
        window.gapi.load('auth2', () => {
          const auth2 = window.gapi.auth2.init({
            client_id: ENV.APP.GOOGLE_CLIENT_ID
          });

          this.set('gapi_auth2', auth2);
        });
      } else {
        const auth2 = window.gapi.auth2.getAuthInstance();

        this.set('gapi_auth2', auth2);
      }
    }
  },

  actions: {
    googleLogin() {
      const gapi_auth2 = this.get('gapi_auth2');
      if (!gapi_auth2) {
        return;
      }

      const notificationTime = ENV.APP.LOGIN_NOTIFICATION_TIME;

      gapi_auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then((result) => {
        Ember.run(() => {
          this.get('session').authenticate('authenticator:google', result.code)
            .then(() => {
              this.get('notify').success(this.get('i18n').t('login.success'), {closeAfter: notificationTime});
              Ember.run.later(() => {
                this.sendAction('onLogin');
              }, notificationTime);
            })
            .catch(() => {
              this.get('notify').alert(this.get('i18n').t('login.error'), {closeAfter: notificationTime});
            });
        });
      });
    }
  }
});
