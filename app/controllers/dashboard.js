import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service(),
  i18n: Ember.inject.service(),

  actions: {
    logout() {
      this.get('session').invalidate();
      this.transitionToRoute('login');
    }
  }
});
