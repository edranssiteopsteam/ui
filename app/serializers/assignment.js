import JSONAPISerializer from 'ember-data/serializers/json-api';
import ENV from '../config/environment';

export default JSONAPISerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    let normalized = null;

    if (payload.items) {
      normalized = this._normalizeAssignments(payload.items);
    } else {
      normalized = this._normalizeAssignments(payload);
    }

    if (payload.meta) {
      normalized.meta = payload.meta;
    }

    return normalized;
  },

  _normalizeAssignments(assignments) {
    return {
      data: assignments.map((assignment) => {
        return {
          type: 'assignment',
          id: assignment._id,
          attributes: {
            date: assignment.date,
            from: assignment.from.email || assignment.from,
            from_image_url: assignment.from.image_url || ENV.APP.USER_DEFAULT_IMAGE,
            to: assignment.to.email || assignment.to,
            to_image_url: assignment.to.image_url || ENV.APP.USER_DEFAULT_IMAGE,
            category: assignment.category,
            comment: assignment.comment
          }
        };
      })
    };
  }
});
