import JSONAPISerializer from 'ember-data/serializers/json-api';

export default JSONAPISerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    return {
      data: {
        type: "user",
        id: payload._id,
        attributes: {
          email: payload.email,
          first_name: payload.first_name,
          last_name: payload.last_name,
          image_url: payload.image_url,
          stars_available: payload.given,
          stars_received: payload.received
        }
      }
    };
  }
});
