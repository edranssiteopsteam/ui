import JSONAPISerializer from 'ember-data/serializers/json-api';

export default JSONAPISerializer.extend({
  normalizeResponse(store, primaryModelClass, payload) {
    return {
      data: payload.map((category) => {
        return {
          type: 'category',
          id: category._id,
          attributes: {
            title: category.title,
            icon: category.icon,
            color: category.color
          }
        };
      })
    };
  }
});
