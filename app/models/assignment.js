import Model from 'ember-data/model';
import attr from 'ember-data/attr';

export default Model.extend({
  date: attr('date'),
  from: attr('string'),
  from_image_url: attr('string'),
  to: attr('string'),
  to_image_url: attr('string'),
  category: attr(),
  comment: attr('string')
});
