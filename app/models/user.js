import Ember from 'ember';
import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import ENV from '../config/environment';

export default Model.extend({
  userService: Ember.inject.service(),

  email: attr('string'),
  first_name: attr('string'),
  last_name: attr('string'),
  image_url: attr('string', {defaultValue: ENV.APP.USER_DEFAULT_IMAGE}),
  stars_available: attr('number'),
  stars_received: attr('number'),

  full_name: Ember.computed('first_name', 'last_name', function() {
    return this.get('userService').getName(this.getProperties('first_name', 'last_name', 'email'));
  })
});
