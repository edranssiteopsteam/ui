import Ember from 'ember';
import OAuth2PasswordGrant from 'ember-simple-auth/authenticators/oauth2-password-grant';
import ENV from '../config/environment';

const { RSVP, run } = Ember;

export default OAuth2PasswordGrant.extend({
  session: Ember.inject.service(),
  google_code_endpoint: ENV.APP.GOOGLE_CODE_ENDPOINT,

  authenticate(code) {
    return new RSVP.Promise((resolve, reject) => {
      const data = { 'code': code };
      const google_code_endpoint = this.get('google_code_endpoint');

      Ember.$.ajax({
        url: google_code_endpoint,
        data: JSON.stringify(data),
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json'
      }).then((response) => {
        run(() => { resolve(response); });
      }, (xhr) => {
        run(null, reject, xhr.responseJSON || xhr.responseText);
      });
    });
  },
});
