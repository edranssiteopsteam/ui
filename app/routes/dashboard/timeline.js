import Ember from 'ember';
import InfinityRoute from 'ember-infinity/mixins/route';

export default Ember.Route.extend(InfinityRoute, {
  model() {
    return this.infinityModel('assignment', {perPage: 10, startingPage: 1});
  },

  afterInfinityModel(assigments) {
    const store = this.get('store');
    const users = store.peekAll('user');
    assigments.map((assignment) => {
      const categoryId = assignment.get('category');
      assignment.set('category', store.peekRecord('category', categoryId));

      const fromUser = users.findBy('email', assignment.get('from'));
      if (fromUser) {
        assignment.set('from', fromUser.get('full_name'));
      }

      const toUser = users.findBy('email', assignment.get('to'));
      if (toUser) {
        assignment.set('to', toUser.get('full_name'));
      }

      return assignment;
    });
  }
});
