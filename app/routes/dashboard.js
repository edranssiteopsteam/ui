import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
  session: Ember.inject.service(),

  activate() {
    this._super(...arguments);

    if (!this.get('session').isAuthenticated) {
      this.transitionTo('login');
    }
  },

  afterModel() {
    this.transitionTo('dashboard.timeline');
  },

  model() {
    const store = this.get('store');
    return {
      user: store.queryRecord('user', {}),
      categories: store.findAll('category')
    };
  },

  actions: {
    error(error) {
      if(error) {
        switch(error.errors[0].status) {
          case '401':
            this.get('session').invalidate();
            return this.transitionTo('login');
          default:
            console.log(error);
            break;
        }
      }
    }
  }

});

