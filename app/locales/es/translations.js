export default {
  'title': 'EstrellaMe',

  'login': {
    'success': 'Inicio de sesión exitoso!',
    'error': 'Error al iniciar la sesión'
  },

  'assign': {
    'send': 'Enviar',
    'search': 'Encuentra personas por nombre o correo',
    'comment': 'Ingresa un comentario aqui',
    'success': 'Enviaste una estrella a {{user}} por {{category}}',
    'error': 'Error al enviar la estrella'
  },

  'dashboard': {
    'profile': 'Perfil',
    'logout': 'Cerrar Sesión',
    'remaining_month': 'ESTRELLAS DISPONIBLES ESTE MES',
    'loading': 'Loading...'
  },

  'Teamwork': 'Trabajo En Equipo',
  'Innovation': 'Innovacion',
  'Achievement': 'Logros Alcanzados',
  'Passion': 'Pasion',
  'Leadership': 'Liderazgo',
  'Collaboration': 'Colaboracion',
  'Continuous Improvement': 'Mejora Continua',
  'Agility': 'Agilidad',
  'Determination': 'Determinacion',
  'Efficiency': 'Eficiencia',
  'Think Big': 'Pensar En Grande'
};
