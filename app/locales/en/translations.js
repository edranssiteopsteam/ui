export default {
  'title': 'StarMe',

  'login': {
    'success': 'Login Success!',
    'error': 'Login Error!'
  },

  'assign': {
    'send': 'Send',
    'search': 'Search people by name or email',
    'comment': 'Insert your comment here',
    'success': 'You sent a star to {{user}} for {{category}}',
    'error': 'Error sending the star'
  },

  'dashboard': {
    'profile': 'User Profile',
    'logout': 'Logout',
    'remaining_month': 'REMAINING STAR THIS MONTH',
    'loading': 'Cargando...'
  },

  'Teamwork': 'Teamwork',
  'Innovation': 'Innovation',
  'Achievement': 'Achievement',
  'Passion': 'Passion',
  'Leadership': 'Leadership',
  'Collaboration': 'Collaboration',
  'Continuous Improvement': 'Collaboration Improvement',
  'Agility': 'Agility',
  'Determination': 'Determination',
  'Efficiency': 'Efficiency',
  'Think Big': 'Think Big'
};
